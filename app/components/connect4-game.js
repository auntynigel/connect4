import Ember from 'ember';

export default Ember.Component.extend({
    playing: false,
    winner: undefined,
    draw: false,

    init: function() {
    this._super(...arguments);
    // https://www.freesound.org/people/rioforce/sounds/233640/ "brick-fall.mp3"
    createjs.Sound.registerSound("assets/sounds/brick-fall.mp3", "falling");
    // "Happy 8bit Loop 01" by Tristan Lohengrin : http://tristanlohengrin.wix.com/studio "happy-loop.mp3"
    createjs.Sound.registerSound("assets/sounds/happy-loop.mp3", "intro");
    // https://www.freesound.org/people/RADIY/sounds/213149/ "bonus-effect.mp3"
    createjs.Sound.registerSound("assets/sounds/brick-fall.mp3", "click");
    // https://www.freesound.org/people/deraj/sounds/202541/ "end.wav" trimmed to only use first 3 seconds of clip
    createjs.Sound.registerSound("assets/sounds/end.wav", "end");
},



    didInsertElement: function() {
        var stage = new createjs.Stage(this.$('#stage')[0]);

        // Draw the game board
        var board = new createjs.Shape();
        var graphics = board.graphics;
        graphics.beginFill('#ffffff');
        // Horizontal lines
        graphics.drawRect(0, 49, 350, 2);
        graphics.drawRect(0, 99, 350, 2);
        graphics.drawRect(0, 149, 350, 2);
        graphics.drawRect(0, 199, 350, 2);
        graphics.drawRect(0, 249, 350, 2);
        graphics.drawRect(0, 299, 350, 2);

        // vertical lines
        graphics.drawRect(0, 0, 2, 300);
        graphics.drawRect(49, 0, 2, 300);
        graphics.drawRect(99, 0, 2, 300);
        graphics.drawRect(149, 0, 2, 300);
        graphics.drawRect(199, 0, 2, 300);
        graphics.drawRect(249, 0, 2, 300);
        graphics.drawRect(299, 0, 2, 300);
        graphics.drawRect(349, 0, 2, 300);

        board.x = 40;
        board.y = 40;
        board.alpha = 0;
        this.set('board', board);
        stage.addChild(board);

        // Add game markers
        var markers = {
            'red': [],
            'yellow': []
        };



        for(var ix = 0; ix <= 21; ix++) {
            // Circle markers represents yellow token
            var circleMarker = new createjs.Shape();
            graphics = circleMarker.graphics;
            graphics.beginFill('#e7e746');
            graphics.drawCircle(0, 0, 15);
            graphics.beginStroke('#7a7c36');
            graphics.setStrokeStyle(5);
            graphics.drawCircle(0, 0, 20);
            circleMarker.visible = false;
            stage.addChild(circleMarker);
            markers.yellow.push(circleMarker);
        }

        for(var o = 0; o <= 21; o++) {
            // cross markers represents red token
            var crossMarker = new createjs.Shape();
            graphics = crossMarker.graphics;
            graphics.beginFill('#e70b10');
            graphics.drawCircle(0, 0, 15);
            graphics.beginStroke('#7c1916');
            graphics.setStrokeStyle(5);
            graphics.drawCircle(0, 0, 20);
            crossMarker.visible = false;
            stage.addChild(crossMarker);
            markers.red.push(crossMarker);
        }

        createjs.Ticker.addEventListener("tick", stage);
        this.set('markers', markers);
        this.set('stage', stage);
    },
    // End didInsert


    // Event listener for click event on screen
    click: function(ev) {
        if(this.get('playing') && !this.get('winner')) {
            if(ev.offsetX >= 20 && ev.offsetY >= 20 && ev.offsetX < 480 && ev.offsetY < 340) {
                var x = Math.floor((ev.offsetX - 20) / 50);
                var y = Math.floor((ev.offsetY - 20) / 50);
                var state = this.get('state');
                if(!state[x][y]) {
                  createjs.Sound.play("click");
                    var player = this.get('player');
                    state[x][y] = player;

                    var move_count = this.get('moves')[player];
                    var marker = this.get('markers')[player][move_count];
                    marker.visible = true;
                    if (player === 'red') {
                        marker.x = 65 + x * 50;
                        marker.y = 65 + y * 50;
                    } else {
                        marker.x = 65 + x * 50;
                        marker.y = 65 + y * 50;
                    }

                    this.check_winner();
                    this.get('moves')[player] = move_count + 1;
                    if (player === 'red') {
                        this.set('player', 'yellow');
                    } else {
                        this.set('player', 'red');
                    }
                    this.get('stage').update();
                }
            } //Close offset x&y
        } // Close if playing and !winner
    },

    check_winner: function(){
      var patterns = [
        // Vertical winning positions
        // column 1 (Left to right)
        [[0,0],[0,1],[0,2],[0,3]],
        [[0,1],[0,2],[0,3],[0,4]],
        [[0,2],[0,3],[0,4],[0,5]],
        // column 2
        [[1,0],[1,1],[1,2],[1,3]],
        [[1,1],[1,2],[1,3],[1,4]],
        [[1,2],[1,3],[1,4],[1,5]],
        // column 3
        [[2,0],[2,1],[2,2],[2,3]],
        [[2,1],[2,2],[2,3],[2,4]],
        [[2,2],[2,3],[2,4],[2,5]],
        // column 4
        [[3,0],[3,1],[3,2],[3,3]],
        [[3,1],[3,2],[3,3],[3,4]],
        [[3,2],[3,3],[3,4],[3,5]],
        // column 5
        [[4,0],[4,1],[4,2],[4,3]],
        [[4,1],[4,2],[4,3],[4,4]],
        [[4,2],[4,3],[4,4],[4,5]],
        // column 6
        [[5,0],[5,1],[5,2],[5,3]],
        [[5,1],[5,2],[5,3],[5,4]],
        [[5,2],[5,3],[5,4],[5,5]],
        // column 7
        [[6,0],[6,1],[6,2],[6,3]],
        [[6,1],[6,2],[6,3],[6,4]],
        [[6,2],[6,3],[6,4],[6,5]],

        // Horizontal winning locations
        // row 1 (top to bottom)
        [[0,0],[1,0],[2,0],[3,0]],
        [[1,0],[2,0],[3,0],[4,0]],
        [[2,0],[3,0],[4,0],[5,0]],
        [[3,0],[4,0],[5,0],[6,0]],
        // row 2
        [[0,1],[1,1],[2,1],[3,1]],
        [[1,1],[2,1],[3,1],[4,1]],
        [[2,1],[3,1],[3,1],[4,1]],
        [[3,1],[4,1],[5,1],[6,1]],
        // row 3
        [[0,2],[1,2],[2,2],[3,2]],
        [[1,2],[2,2],[3,2],[4,2]],
        [[2,2],[3,2],[3,2],[4,2]],
        [[3,2],[4,2],[5,2],[6,2]],
        // row 4
        [[0,3],[1,3],[2,3],[3,3]],
        [[1,3],[2,3],[3,3],[4,3]],
        [[2,3],[3,3],[3,3],[4,3]],
        [[3,3],[4,3],[5,3],[6,3]],
        // row 5
        [[0,4],[1,4],[2,4],[3,4]],
        [[1,4],[2,4],[3,4],[4,4]],
        [[2,4],[3,4],[3,4],[4,4]],
        [[3,4],[4,4],[5,4],[6,4]],
        // row 6
        [[0,5],[1,5],[2,5],[3,5]],
        [[1,5],[2,5],[3,5],[4,5]],
        [[2,5],[3,5],[3,5],[4,5]],
        [[3,5],[4,5],[5,5],[6,5]],
        // row 7
        [[0,6],[1,6],[2,6],[3,6]],
        [[1,6],[2,6],[3,6],[4,6]],
        [[2,6],[3,6],[3,6],[4,6]],
        [[3,6],[4,6],[5,6],[6,6]],
        // Diagonal positions left > right board
        // column 1 (columns 1-4)
        [[0,0],[1,1],[2,2],[3,3]],
        [[0,1],[1,2],[2,3],[3,4]],
        [[0,2],[1,3],[2,4],[3,5]],
        // column 2 (columns 2-5)
        [[1,0],[2,1],[3,2],[4,3]],
        [[1,1],[2,2],[3,3],[4,4]],
        [[1,2],[2,3],[3,4],[4,5]],
        // column 3 (columns 3-6)
        [[2,0],[3,1],[4,2],[5,3]],
        [[2,1],[3,2],[4,3],[5,4]],
        [[2,2],[3,3],[4,4],[5,5]],
        // column 4 (columns 4-7)
        [[3,0],[4,1],[5,2],[6,3]],
        [[3,1],[4,2],[5,3],[6,4]],
        [[3,2],[4,3],[5,4],[6,5]],
        // Diagonal positions left < right board
        // column 7 (columns 4-7)
        [[6,0],[5,1],[4,2],[3,3]],
        [[6,1],[5,2],[4,3],[3,4]],
        [[6,2],[5,3],[4,4],[3,5]],
        // column 6 (columns 3-6)
        [[5,0],[4,1],[3,2],[2,3]],
        [[5,1],[4,2],[3,3],[2,4]],
        [[5,2],[4,3],[3,4],[2,5]],
        // column 5 (columns 2-5)
        [[4,0],[3,1],[2,2],[1,3]],
        [[4,1],[3,2],[2,3],[1,4]],
        [[4,2],[3,3],[2,4],[1,5]],
        // column 4 (columns 1-4)
        [[3,0],[2,1],[1,2],[0,3]],
        [[3,1],[2,2],[1,3],[0,4]],
        [[3,2],[2,3],[1,4],[0,5]],
      ];

      var state = this.get('state');
      // Run over patterns array to check for winning positions
      for(var pidx = 0; pidx < patterns.length; pidx++) {
        var pattern = patterns[pidx];
        var winner = state[pattern[0][0]][pattern[0][1]];
        if(winner) {
          for(var idx = 1; idx < pattern.length; idx++) {
            if(winner != state[pattern[idx][0]][pattern[idx][1]]) {
              winner = undefined;
              break;
            } // Close if winner != state
          } // Close for pattern.length
          if(winner) {
            this.set('winner', winner);
            break;
          }
        }
      }
    },

    actions: {
        start: function() {
            // Animate board visibility
            var board = this.get('board');
            board.alpha = 0;
            createjs.Tween.get(board).to({alpha: 1}, 1000);
            createjs.Sound.play("intro");
            // Define variable states
            this.set('playing', true);
            this.set('winner', undefined);
            this.set('state', [
              // Rows, not columns
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined]
            ]);
            // Reset moves to 0 for start of game
            this.set('moves', {'red': 0, 'yellow': 0});
            // Set markers visibility to false
            this.set('player', 'red');
            var markers = this.get('markers');
            for(var id = 0; id <= 21; id++) {
                markers.red[id].visible = false;
                markers.yellow[id].visible = false;
            }
            this.get('stage').update();
        },

        reset: function(){
          var board = this.get('board');
          // Define variable states
          this.set('playing', true);
          this.set('winner', undefined);
          // Reset moves to 0 for start of game
          this.set('moves', {'red': 0, 'yellow': 0});
          // Set markers visibility to false
          this.set('player', 'red');
          this.set('state', [
            // Rows, not columns
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined]
          ]);

          // if tokens played, then animate off screen
          if(this.get('playing')) {
            var markers = this.get('markers');
            for(var idx = 0; idx <= 21; idx++) {
              createjs.Sound.play("end");
              createjs.Tween.get(markers.red[idx]).to({y: 600}, 500);
              createjs.Tween.get(markers.yellow[idx]).to({y: 600}, 500);
            } // close for idx21
            // No tokens played then immediately change board alpha
            createjs.Tween.this(board).to({alpha: 0}, 1000);
          } // close if playing
          // Set markers invisible
          for(var id = 0; id <= 21; id++) {
              this.markers.red[id].visible = false;
              this.markers.yellow[id].visible = false;
          }

        } // Close function

    }

});
