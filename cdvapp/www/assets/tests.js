'use strict';

define('coursework/tests/app.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | app.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass jshint.');
  });
});
define('coursework/tests/components/connect4-game.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/connect4-game.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/connect4-game.js should pass jshint.\ncomponents/connect4-game.js: line 227, col 25, Expected \'!==\' and instead saw \'!=\'.\ncomponents/connect4-game.js: line 11, col 5, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 13, col 5, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 15, col 5, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 17, col 5, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 23, col 25, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 26, col 25, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 63, col 36, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 77, col 35, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 89, col 9, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 104, col 19, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 245, col 13, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 246, col 13, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 296, col 15, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 297, col 15, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 298, col 15, \'createjs\' is not defined.\ncomponents/connect4-game.js: line 301, col 13, \'createjs\' is not defined.\n\n17 errors');
  });
});
define('coursework/tests/helpers/destroy-app', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = destroyApp;

  function destroyApp(application) {
    _ember['default'].run(application, 'destroy');
  }
});
define('coursework/tests/helpers/destroy-app.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | helpers/destroy-app.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass jshint.');
  });
});
define('coursework/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'ember', 'coursework/tests/helpers/start-app', 'coursework/tests/helpers/destroy-app'], function (exports, _qunit, _ember, _courseworkTestsHelpersStartApp, _courseworkTestsHelpersDestroyApp) {
  var Promise = _ember['default'].RSVP.Promise;

  exports['default'] = function (name) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _courseworkTestsHelpersStartApp['default'])();

        if (options.beforeEach) {
          return options.beforeEach.apply(this, arguments);
        }
      },

      afterEach: function afterEach() {
        var _this = this;

        var afterEach = options.afterEach && options.afterEach.apply(this, arguments);
        return Promise.resolve(afterEach).then(function () {
          return (0, _courseworkTestsHelpersDestroyApp['default'])(_this.application);
        });
      }
    });
  };
});
define('coursework/tests/helpers/module-for-acceptance.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | helpers/module-for-acceptance.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass jshint.');
  });
});
define('coursework/tests/helpers/resolver', ['exports', 'coursework/resolver', 'coursework/config/environment'], function (exports, _courseworkResolver, _courseworkConfigEnvironment) {

  var resolver = _courseworkResolver['default'].create();

  resolver.namespace = {
    modulePrefix: _courseworkConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _courseworkConfigEnvironment['default'].podModulePrefix
  };

  exports['default'] = resolver;
});
define('coursework/tests/helpers/resolver.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | helpers/resolver.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/resolver.js should pass jshint.');
  });
});
define('coursework/tests/helpers/start-app', ['exports', 'ember', 'coursework/app', 'coursework/config/environment'], function (exports, _ember, _courseworkApp, _courseworkConfigEnvironment) {
  exports['default'] = startApp;

  function startApp(attrs) {
    var application = undefined;

    var attributes = _ember['default'].merge({}, _courseworkConfigEnvironment['default'].APP);
    attributes = _ember['default'].merge(attributes, attrs); // use defaults, but you can override;

    _ember['default'].run(function () {
      application = _courseworkApp['default'].create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
    });

    return application;
  }
});
define('coursework/tests/helpers/start-app.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | helpers/start-app.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass jshint.');
  });
});
define('coursework/tests/integration/components/connect4-game-test', ['exports', 'ember-qunit'], function (exports, _emberQunit) {

  (0, _emberQunit.moduleForComponent)('connect4-game', 'Integration | Component | connect4 game', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@2.9.1',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 17
            }
          }
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'connect4-game', ['loc', [null, [1, 0], [1, 17]]], 0, 0, 0, 0]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@2.9.1',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@2.9.1',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'connect4-game', [], [], 0, null, ['loc', [null, [2, 4], [4, 22]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('coursework/tests/integration/components/connect4-game-test.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | integration/components/connect4-game-test.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/connect4-game-test.js should pass jshint.');
  });
});
define('coursework/tests/resolver.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | resolver.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass jshint.');
  });
});
define('coursework/tests/router.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | router.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass jshint.');
  });
});
define('coursework/tests/routes/game.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/game.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/game.js should pass jshint.');
  });
});
define('coursework/tests/test-helper', ['exports', 'coursework/tests/helpers/resolver', 'ember-qunit'], function (exports, _courseworkTestsHelpersResolver, _emberQunit) {

  (0, _emberQunit.setResolver)(_courseworkTestsHelpersResolver['default']);
});
define('coursework/tests/test-helper.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | test-helper.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass jshint.');
  });
});
/* jshint ignore:start */

require('coursework/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;

/* jshint ignore:end */
//# sourceMappingURL=tests.map
