"use strict";

/* jshint ignore:start */



/* jshint ignore:end */

define('coursework/app', ['exports', 'ember', 'coursework/resolver', 'ember-load-initializers', 'coursework/config/environment'], function (exports, _ember, _courseworkResolver, _emberLoadInitializers, _courseworkConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _courseworkConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _courseworkConfigEnvironment['default'].podModulePrefix,
    Resolver: _courseworkResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _courseworkConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});
define("coursework/components/connect4-game", ["exports", "ember"], function (exports, _ember) {
    exports["default"] = _ember["default"].Component.extend({
        playing: false,
        winner: undefined,
        draw: false,

        init: function init() {
            this._super.apply(this, arguments);
            // https://www.freesound.org/people/rioforce/sounds/233640/ "brick-fall.mp3"
            createjs.Sound.registerSound("assets/sounds/brick-fall.mp3", "falling");
            // "Happy 8bit Loop 01" by Tristan Lohengrin : http://tristanlohengrin.wix.com/studio "happy-loop.mp3"
            createjs.Sound.registerSound("assets/sounds/happy-loop.mp3", "intro");
            // https://www.freesound.org/people/RADIY/sounds/213149/ "bonus-effect.mp3"
            createjs.Sound.registerSound("assets/sounds/brick-fall.mp3", "click");
            // https://www.freesound.org/people/deraj/sounds/202541/ "end.wav" trimmed to only use first 3 seconds of clip
            createjs.Sound.registerSound("assets/sounds/end.wav", "end");
        },

        didInsertElement: function didInsertElement() {
            var stage = new createjs.Stage(this.$('#stage')[0]);

            // Draw the game board
            var board = new createjs.Shape();
            var graphics = board.graphics;
            graphics.beginFill('#ffffff');
            // Horizontal lines
            graphics.drawRect(0, 49, 350, 2);
            graphics.drawRect(0, 99, 350, 2);
            graphics.drawRect(0, 149, 350, 2);
            graphics.drawRect(0, 199, 350, 2);
            graphics.drawRect(0, 249, 350, 2);
            graphics.drawRect(0, 299, 350, 2);

            // vertical lines
            graphics.drawRect(0, 0, 2, 300);
            graphics.drawRect(49, 0, 2, 300);
            graphics.drawRect(99, 0, 2, 300);
            graphics.drawRect(149, 0, 2, 300);
            graphics.drawRect(199, 0, 2, 300);
            graphics.drawRect(249, 0, 2, 300);
            graphics.drawRect(299, 0, 2, 300);
            graphics.drawRect(349, 0, 2, 300);

            board.x = 40;
            board.y = 40;
            board.alpha = 0;
            this.set('board', board);
            stage.addChild(board);

            // Add game markers
            var markers = {
                'red': [],
                'yellow': []
            };

            for (var ix = 0; ix <= 21; ix++) {
                // Circle markers represents yellow token
                var circleMarker = new createjs.Shape();
                graphics = circleMarker.graphics;
                graphics.beginFill('#e7e746');
                graphics.drawCircle(0, 0, 15);
                graphics.beginStroke('#7a7c36');
                graphics.setStrokeStyle(5);
                graphics.drawCircle(0, 0, 20);
                circleMarker.visible = false;
                stage.addChild(circleMarker);
                markers.yellow.push(circleMarker);
            }

            for (var o = 0; o <= 21; o++) {
                // cross markers represents red token
                var crossMarker = new createjs.Shape();
                graphics = crossMarker.graphics;
                graphics.beginFill('#e70b10');
                graphics.drawCircle(0, 0, 15);
                graphics.beginStroke('#7c1916');
                graphics.setStrokeStyle(5);
                graphics.drawCircle(0, 0, 20);
                crossMarker.visible = false;
                stage.addChild(crossMarker);
                markers.red.push(crossMarker);
            }

            createjs.Ticker.addEventListener("tick", stage);
            this.set('markers', markers);
            this.set('stage', stage);
        },
        // End didInsert

        // Event listener for click event on screen
        click: function click(ev) {
            if (this.get('playing') && !this.get('winner')) {
                if (ev.offsetX >= 20 && ev.offsetY >= 20 && ev.offsetX < 480 && ev.offsetY < 340) {
                    var x = Math.floor((ev.offsetX - 20) / 50);
                    var y = Math.floor((ev.offsetY - 20) / 50);
                    var state = this.get('state');
                    if (!state[x][y]) {
                        createjs.Sound.play("click");
                        var player = this.get('player');
                        state[x][y] = player;

                        var move_count = this.get('moves')[player];
                        var marker = this.get('markers')[player][move_count];
                        marker.visible = true;
                        if (player === 'red') {
                            marker.x = 65 + x * 50;
                            marker.y = 65 + y * 50;
                        } else {
                            marker.x = 65 + x * 50;
                            marker.y = 65 + y * 50;
                        }

                        this.check_winner();
                        this.get('moves')[player] = move_count + 1;
                        if (player === 'red') {
                            this.set('player', 'yellow');
                        } else {
                            this.set('player', 'red');
                        }
                        this.get('stage').update();
                    }
                } //Close offset x&y
            } // Close if playing and !winner
        },

        check_winner: function check_winner() {
            var patterns = [
            // Vertical winning positions
            // column 1 (Left to right)
            [[0, 0], [0, 1], [0, 2], [0, 3]], [[0, 1], [0, 2], [0, 3], [0, 4]], [[0, 2], [0, 3], [0, 4], [0, 5]],
            // column 2
            [[1, 0], [1, 1], [1, 2], [1, 3]], [[1, 1], [1, 2], [1, 3], [1, 4]], [[1, 2], [1, 3], [1, 4], [1, 5]],
            // column 3
            [[2, 0], [2, 1], [2, 2], [2, 3]], [[2, 1], [2, 2], [2, 3], [2, 4]], [[2, 2], [2, 3], [2, 4], [2, 5]],
            // column 4
            [[3, 0], [3, 1], [3, 2], [3, 3]], [[3, 1], [3, 2], [3, 3], [3, 4]], [[3, 2], [3, 3], [3, 4], [3, 5]],
            // column 5
            [[4, 0], [4, 1], [4, 2], [4, 3]], [[4, 1], [4, 2], [4, 3], [4, 4]], [[4, 2], [4, 3], [4, 4], [4, 5]],
            // column 6
            [[5, 0], [5, 1], [5, 2], [5, 3]], [[5, 1], [5, 2], [5, 3], [5, 4]], [[5, 2], [5, 3], [5, 4], [5, 5]],
            // column 7
            [[6, 0], [6, 1], [6, 2], [6, 3]], [[6, 1], [6, 2], [6, 3], [6, 4]], [[6, 2], [6, 3], [6, 4], [6, 5]],

            // Horizontal winning locations
            // row 1 (top to bottom)
            [[0, 0], [1, 0], [2, 0], [3, 0]], [[1, 0], [2, 0], [3, 0], [4, 0]], [[2, 0], [3, 0], [4, 0], [5, 0]], [[3, 0], [4, 0], [5, 0], [6, 0]],
            // row 2
            [[0, 1], [1, 1], [2, 1], [3, 1]], [[1, 1], [2, 1], [3, 1], [4, 1]], [[2, 1], [3, 1], [3, 1], [4, 1]], [[3, 1], [4, 1], [5, 1], [6, 1]],
            // row 3
            [[0, 2], [1, 2], [2, 2], [3, 2]], [[1, 2], [2, 2], [3, 2], [4, 2]], [[2, 2], [3, 2], [3, 2], [4, 2]], [[3, 2], [4, 2], [5, 2], [6, 2]],
            // row 4
            [[0, 3], [1, 3], [2, 3], [3, 3]], [[1, 3], [2, 3], [3, 3], [4, 3]], [[2, 3], [3, 3], [3, 3], [4, 3]], [[3, 3], [4, 3], [5, 3], [6, 3]],
            // row 5
            [[0, 4], [1, 4], [2, 4], [3, 4]], [[1, 4], [2, 4], [3, 4], [4, 4]], [[2, 4], [3, 4], [3, 4], [4, 4]], [[3, 4], [4, 4], [5, 4], [6, 4]],
            // row 6
            [[0, 5], [1, 5], [2, 5], [3, 5]], [[1, 5], [2, 5], [3, 5], [4, 5]], [[2, 5], [3, 5], [3, 5], [4, 5]], [[3, 5], [4, 5], [5, 5], [6, 5]],
            // row 7
            [[0, 6], [1, 6], [2, 6], [3, 6]], [[1, 6], [2, 6], [3, 6], [4, 6]], [[2, 6], [3, 6], [3, 6], [4, 6]], [[3, 6], [4, 6], [5, 6], [6, 6]],
            // Diagonal positions left > right board
            // column 1 (columns 1-4)
            [[0, 0], [1, 1], [2, 2], [3, 3]], [[0, 1], [1, 2], [2, 3], [3, 4]], [[0, 2], [1, 3], [2, 4], [3, 5]],
            // column 2 (columns 2-5)
            [[1, 0], [2, 1], [3, 2], [4, 3]], [[1, 1], [2, 2], [3, 3], [4, 4]], [[1, 2], [2, 3], [3, 4], [4, 5]],
            // column 3 (columns 3-6)
            [[2, 0], [3, 1], [4, 2], [5, 3]], [[2, 1], [3, 2], [4, 3], [5, 4]], [[2, 2], [3, 3], [4, 4], [5, 5]],
            // column 4 (columns 4-7)
            [[3, 0], [4, 1], [5, 2], [6, 3]], [[3, 1], [4, 2], [5, 3], [6, 4]], [[3, 2], [4, 3], [5, 4], [6, 5]]];

            var state = this.get('state');
            // Run over patterns array to check for winning positions
            for (var pidx = 0; pidx < patterns.length; pidx++) {
                var pattern = patterns[pidx];
                var winner = state[pattern[0][0]][pattern[0][1]];
                if (winner) {
                    for (var idx = 1; idx < pattern.length; idx++) {
                        if (winner != state[pattern[idx][0]][pattern[idx][1]]) {
                            winner = undefined;
                            break;
                        } // Close if winner != state
                    } // Close for pattern.length
                    if (winner) {
                        this.set('winner', winner);
                        break;
                    }
                }
            }
        },

        actions: {
            start: function start() {
                // Animate board visibility
                var board = this.get('board');
                board.alpha = 0;
                createjs.Tween.get(board).to({ alpha: 1 }, 1000);
                createjs.Sound.play("intro");
                // Define variable states
                this.set('playing', true);
                this.set('winner', undefined);
                this.set('state', [
                // Rows, not columns
                [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined]]);
                // Reset moves to 0 for start of game
                this.set('moves', { 'red': 0, 'yellow': 0 });
                // Set markers visibility to false
                this.set('player', 'red');
                var markers = this.get('markers');
                for (var id = 0; id <= 21; id++) {
                    markers.red[id].visible = false;
                    markers.yellow[id].visible = false;
                }
                this.get('stage').update();
            },

            reset: function reset() {
                var board = this.get('board');
                // Define variable states
                this.set('playing', true);
                this.set('winner', undefined);
                // Reset moves to 0 for start of game
                this.set('moves', { 'red': 0, 'yellow': 0 });
                // Set markers visibility to false
                this.set('player', 'red');
                this.set('state', [
                // Rows, not columns
                [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined]]);

                // if tokens played, then animate off screen
                if (this.get('playing')) {
                    var markers = this.get('markers');
                    for (var idx = 0; idx <= 21; idx++) {
                        createjs.Sound.play("end");
                        createjs.Tween.get(markers.red[idx]).to({ y: 600 }, 500);
                        createjs.Tween.get(markers.yellow[idx]).to({ y: 600 }, 500);
                    } // close for idx21
                    // No tokens played then immediately change board alpha
                    createjs.Tween["this"](board).to({ alpha: 0 }, 1000);
                } // close if playing
                // Set markers invisible
                for (var id = 0; id <= 21; id++) {
                    this.markers.red[id].visible = false;
                    this.markers.yellow[id].visible = false;
                }
            } // Close function

        }

    });
});
define('coursework/helpers/app-version', ['exports', 'ember', 'coursework/config/environment'], function (exports, _ember, _courseworkConfigEnvironment) {
  exports.appVersion = appVersion;
  var version = _courseworkConfigEnvironment['default'].APP.version;

  function appVersion() {
    return version;
  }

  exports['default'] = _ember['default'].Helper.helper(appVersion);
});
define('coursework/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _emberInflectorLibHelpersPluralize) {
  exports['default'] = _emberInflectorLibHelpersPluralize['default'];
});
define('coursework/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _emberInflectorLibHelpersSingularize) {
  exports['default'] = _emberInflectorLibHelpersSingularize['default'];
});
define('coursework/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'coursework/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _courseworkConfigEnvironment) {
  var _config$APP = _courseworkConfigEnvironment['default'].APP;
  var name = _config$APP.name;
  var version = _config$APP.version;
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(name, version)
  };
});
define('coursework/initializers/container-debug-adapter', ['exports', 'ember-resolver/container-debug-adapter'], function (exports, _emberResolverContainerDebugAdapter) {
  exports['default'] = {
    name: 'container-debug-adapter',

    initialize: function initialize() {
      var app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _emberResolverContainerDebugAdapter['default']);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
define('coursework/initializers/data-adapter', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `data-adapter` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'data-adapter',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define('coursework/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data/-private/core'], function (exports, _emberDataSetupContainer, _emberDataPrivateCore) {

  /*
  
    This code initializes Ember-Data onto an Ember application.
  
    If an Ember.js developer defines a subclass of DS.Store on their application,
    as `App.StoreService` (or via a module system that resolves to `service:store`)
    this code will automatically instantiate it and make it available on the
    router.
  
    Additionally, after an application's controllers have been injected, they will
    each have the store made available to them.
  
    For example, imagine an Ember.js application with the following classes:
  
    App.StoreService = DS.Store.extend({
      adapter: 'custom'
    });
  
    App.PostsController = Ember.Controller.extend({
      // ...
    });
  
    When the application is initialized, `App.ApplicationStore` will automatically be
    instantiated, and the instance of `App.PostsController` will have its `store`
    property set to that instance.
  
    Note that this code will only be run if the `ember-application` package is
    loaded. If Ember Data is being used in an environment other than a
    typical application (e.g., node.js where only `ember-runtime` is available),
    this code will be ignored.
  */

  exports['default'] = {
    name: 'ember-data',
    initialize: _emberDataSetupContainer['default']
  };
});
define('coursework/initializers/export-application-global', ['exports', 'ember', 'coursework/config/environment'], function (exports, _ember, _courseworkConfigEnvironment) {
  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_courseworkConfigEnvironment['default'].exportApplicationGlobal !== false) {
      var theGlobal;
      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _courseworkConfigEnvironment['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = _ember['default'].String.classify(_courseworkConfigEnvironment['default'].modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('coursework/initializers/injectStore', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `injectStore` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'injectStore',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define('coursework/initializers/store', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `store` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'store',
    after: 'ember-data',
    initialize: _ember['default'].K
  };
});
define('coursework/initializers/transforms', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `transforms` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'transforms',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define("coursework/instance-initializers/ember-data", ["exports", "ember-data/-private/instance-initializers/initialize-store-service"], function (exports, _emberDataPrivateInstanceInitializersInitializeStoreService) {
  exports["default"] = {
    name: "ember-data",
    initialize: _emberDataPrivateInstanceInitializersInitializeStoreService["default"]
  };
});
define('coursework/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  exports['default'] = _emberResolver['default'];
});
define('coursework/router', ['exports', 'ember', 'coursework/config/environment'], function (exports, _ember, _courseworkConfigEnvironment) {

  var Router = _ember['default'].Router.extend({
    location: _courseworkConfigEnvironment['default'].locationType,
    rootURL: _courseworkConfigEnvironment['default'].rootURL
  });

  Router.map(function () {
    this.route('game', { path: '/' });
  });

  exports['default'] = Router;
});
define('coursework/routes/game', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('coursework/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _emberAjaxServicesAjax) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberAjaxServicesAjax['default'];
    }
  });
});
define("coursework/templates/application", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 16,
            "column": 10
          }
        },
        "moduleName": "coursework/templates/application.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("section");
        dom.setAttribute(el1, "id", "app");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("header");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("h1");
        var el4 = dom.createTextNode("Connect4");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("article");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("footer");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "float-left");
        var el4 = dom.createTextNode("\n            Powered by Ember.\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "float-right");
        var el4 = dom.createTextNode("\n            built by M.Williams\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [0, 3]), 1, 1);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [6, 8], [6, 18]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("coursework/templates/components/connect4-game", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.1",
            "loc": {
              "source": null,
              "start": {
                "line": 14,
                "column": 8
              },
              "end": {
                "line": 16,
                "column": 8
              }
            },
            "moduleName": "coursework/templates/components/connect4-game.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("          Player ");
            dom.appendChild(el0, el1);
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode(" Has Won!\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
            return morphs;
          },
          statements: [["content", "winner", ["loc", [null, [15, 17], [15, 27]]], 0, 0, 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.1",
          "loc": {
            "source": null,
            "start": {
              "line": 4,
              "column": 4
            },
            "end": {
              "line": 18,
              "column": 4
            }
          },
          "moduleName": "coursework/templates/components/connect4-game.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("\n        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "float-left third");
          var el2 = dom.createTextNode("\n            Player 1: ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "class", "yellow");
          var el3 = dom.createTextNode("Yellow");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "float-right third");
          var el2 = dom.createTextNode("\n            CPU: ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "class", "red");
          var el3 = dom.createTextNode("Red");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n\n        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          dom.setAttribute(el1, "class", "game");
          var el2 = dom.createTextNode("Reset");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element1 = dom.childAt(fragment, [5]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element1);
          morphs[1] = dom.createMorphAt(fragment, 7, 7, contextualElement);
          return morphs;
        },
        statements: [["element", "action", ["reset"], [], ["loc", [null, [13, 29], [13, 49]]], 0, 0], ["block", "if", [["get", "winner", ["loc", [null, [14, 14], [14, 20]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [14, 8], [16, 15]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.1",
          "loc": {
            "source": null,
            "start": {
              "line": 18,
              "column": 4
            },
            "end": {
              "line": 22,
              "column": 4
            }
          },
          "moduleName": "coursework/templates/components/connect4-game.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("\n    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          dom.setAttribute(el1, "class", "game");
          var el2 = dom.createTextNode("Play Connect4");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1]);
          var morphs = new Array(1);
          morphs[0] = dom.createElementMorph(element0);
          return morphs;
        },
        statements: [["element", "action", ["start"], [], ["loc", [null, [20, 25], [20, 45]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 25,
            "column": 0
          }
        },
        "moduleName": "coursework/templates/components/connect4-game.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "text-center");
        var el2 = dom.createTextNode("\n\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("canvas");
        dom.setAttribute(el2, "id", "stage");
        dom.setAttribute(el2, "width", "420");
        dom.setAttribute(el2, "height", "380");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(2);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        morphs[1] = dom.createMorphAt(dom.childAt(fragment, [2]), 1, 1);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]], 0, 0, 0, 0], ["block", "if", [["get", "playing", ["loc", [null, [4, 10], [4, 17]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [4, 4], [22, 11]]]]],
      locals: [],
      templates: [child0, child1]
    };
  })());
});
define("coursework/templates/game", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "coursework/templates/game.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "connect4-game", ["loc", [null, [1, 0], [1, 17]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
/* jshint ignore:start */



/* jshint ignore:end */

/* jshint ignore:start */

define('coursework/config/environment', ['ember'], function(Ember) {
  var prefix = 'coursework';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

/* jshint ignore:end */

/* jshint ignore:start */

if (!runningTests) {
  require("coursework/app")["default"].create({"name":"coursework","version":"0.0.0+74d05a97"});
}

/* jshint ignore:end */
//# sourceMappingURL=coursework.map
